# Asclepios-Client

SSE client is a main component for SSE. It is a web application which uses SSE services such as uploading encrypted data, search/update/delete over the stored encrypted data. 

The details about how to run SSE client in development mode could be reached in [SSE_deployment_manual.pdf](https://gitlab.com/asclepios-project/ssemanual/-/blob/develop/SSE_deployment_manual.pdf). Besides, the details about how to run SSE client in docker container could be found in [SSE_deployment_manual.pdf](https://gitlab.com/asclepios-project/ssemanual/-/blob/develop/SSE_deployment_manual.pdf). Apart from that, the details about how to run SSE client using MiCADO could also be reached in [SSE_deployment_manual.pdf](https://gitlab.com/asclepios-project/ssemanual/-/blob/develop/SSE_deployment_manual.pdf).
